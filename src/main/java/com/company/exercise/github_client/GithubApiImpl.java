package com.company.exercise.github_client;

import java.util.Optional;

class GithubApiImpl implements GithubApi {

    @Override
    public Optional<GithubUser> getGithubUserData(String username) {
        return githubClient.getGithubUserData(username);
    }

    private final Client githubClient;

    public GithubApiImpl(Client githubClient) {
        this.githubClient = githubClient;
    }
}
