package com.company.exercise.github_client;

public class ClientDefaultException extends RuntimeException {

    public ClientDefaultException() {
        super();
    }

    public ClientDefaultException(String message) {
        super(message);
    }
}
