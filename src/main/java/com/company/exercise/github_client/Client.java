package com.company.exercise.github_client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(value = "github", url = "https://api.github.com", decode404 = true)
interface Client {


    @GetMapping(value = "/users/{username}")
    Optional<GithubUser> getGithubUserData(@PathVariable String username);
}
