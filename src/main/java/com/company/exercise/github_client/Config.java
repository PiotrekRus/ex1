package com.company.exercise.github_client;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class Config {
    @Bean
    GithubApi getApi(Client client) {
        return new GithubApiImpl(client);
    }

    @Bean
    ErrorDecoder getErrorDecoder() {
        return new ClientErrorDecoder();
    }
}
