package com.company.exercise.github_client;

import feign.Response;
import feign.codec.ErrorDecoder;

class ClientErrorDecoder implements ErrorDecoder {

    @Override
    public Exception decode(String s, Response response) {
        switch (response.status()) {
            case 403:
                return new Client403Exception("Client api returns Forbidden-403 code");
            default:
                return new ClientDefaultException("Client api error");
        }
    }
}
