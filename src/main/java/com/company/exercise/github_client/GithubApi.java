package com.company.exercise.github_client;

import java.util.Optional;

public interface GithubApi {

    Optional<GithubUser> getGithubUserData(String username);
}
