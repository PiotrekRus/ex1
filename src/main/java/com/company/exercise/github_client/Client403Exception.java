package com.company.exercise.github_client;

public class Client403Exception extends RuntimeException {

    public Client403Exception() {
        super();
    }

    public Client403Exception(String message) {
        super(message);
    }
}
