package com.company.exercise.user;

import com.company.exercise.github_client.Client403Exception;
import com.company.exercise.github_client.ClientDefaultException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

@RestControllerAdvice
class ControllerExceptionHandler {

    @ExceptionHandler({UserNotFoundException.class})
    ResponseEntity<Map<String, String>> handleUserNotFound(UserNotFoundException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("message", ex.getMessage()));
    }

    @ExceptionHandler({Client403Exception.class})
    ResponseEntity<Map<String, String>> handleClient403(Client403Exception ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("message", ex.getMessage()));
    }

    @ExceptionHandler({ClientDefaultException.class})
    ResponseEntity<Map<String, String>> handleClientDefaultException(ClientDefaultException ex) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(Map.of("message", ex.getMessage()));
    }
}
