package com.company.exercise.user;

import com.company.exercise.github_client.GithubApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class UserConfig {

    @Bean
    static UserApi getUserApi(GithubApi githubApi, Repository repository) {
        var databaseApi = getDatabaseApi(repository);
        return new UserApiImpl(githubApi, databaseApi);
    }

    @Bean
    static Repository getRepository() {
        return new DatabaseRepository();
    }

    private static DatabaseApi getDatabaseApi(Repository repository) {
        return new DatabaseApi(repository);
    }

}
