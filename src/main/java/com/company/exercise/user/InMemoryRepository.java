package com.company.exercise.user;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

class InMemoryRepository implements Repository {

    @Override
    public void createOrUpdate(String login) {
        var request = inMemory.values().stream().filter(s -> s.getLogin().equals(login)).findFirst();
        if (request.isPresent()) {
            var requestToUpdate = request.get();
            requestToUpdate.incrementCount();
        } else {
            var id = getLastId() + 1;
            inMemory.put(id, new ApiRequest(login, id));
        }
    }

    @Override
    public Optional<ApiRequest> findByLogin(String login) {
        return inMemory.values().stream().filter(s -> s.getLogin().equals(login)).findFirst();
    }

    private Long getLastId() {
        return inMemory.keySet().stream().mapToLong(v -> v).max().orElse(0);
    }

    private Map<Long, ApiRequest> inMemory = new HashMap<>();
}
