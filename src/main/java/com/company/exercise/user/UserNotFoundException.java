package com.company.exercise.user;

class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("User with given login not found");
    }

    public UserNotFoundException(String message) {
        super(message);
    }
}
