package com.company.exercise.user;

import lombok.experimental.UtilityClass;

@UtilityClass
class UserCalculator {

    static Double calculate(long followers, long publicRepos) {
        if (areParamsInvalid(followers, publicRepos)) {
            return null;
        }
        return 6.0 / followers * (2 + publicRepos);
    }

    private static boolean areParamsInvalid(long followers, long publicRepos) {
        return followers < 1 || publicRepos == -2;
    }
}
