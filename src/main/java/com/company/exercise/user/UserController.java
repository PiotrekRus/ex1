package com.company.exercise.user;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
class UserController {

    @GetMapping("/{login}")
    ResponseEntity<User> getUserData(@PathVariable String login) {
        return ResponseEntity.ok(api.getCalculatedUserInfo(login));
    }

    private final UserApi api;

    public UserController(UserApi api) {
        this.api = api;
    }
}
