package com.company.exercise.user;

class DatabaseApi {

    public void saveRequest(String login) {
        repository.createOrUpdate(login);
    }

    private final Repository repository;

    public DatabaseApi(Repository repository) {
        this.repository = repository;
    }


}
