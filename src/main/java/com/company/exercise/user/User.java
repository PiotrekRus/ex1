package com.company.exercise.user;

import com.company.exercise.github_client.GithubUser;
import lombok.Value;

@Value
class User {
    private Long id;
    private String login;
    private String name;
    private String type;
    private String avatarUrl;
    private String createdAt;
    private Double calculations;

    static User of(GithubUser user) {
        return new User(user.getId(), user.getLogin(), user.getName(), user.getType(), user.getAvatarUrl(),
                user.getCreatedAt(), UserCalculator.calculate(user.getFollowers(), user.getPublicRepos()));
    }
}
