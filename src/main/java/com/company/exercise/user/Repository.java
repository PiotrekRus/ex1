package com.company.exercise.user;

import java.util.Optional;

interface Repository {

    void createOrUpdate(String login);

    Optional<ApiRequest> findByLogin(String login);

}
