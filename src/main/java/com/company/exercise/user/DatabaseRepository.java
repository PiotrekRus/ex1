package com.company.exercise.user;

import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Optional;


class DatabaseRepository implements Repository {

    @PersistenceContext
    EntityManager entityManager;

    @Transactional
    @Override
    public void createOrUpdate(String login) {
        var queryString = "INSERT INTO API_REQUEST ( LOGIN , REQUEST_COUNT ) VALUES (:login, 1) " +
                "ON DUPLICATE KEY UPDATE  REQUEST_COUNT = REQUEST_COUNT +1";
        var query = entityManager.createNativeQuery(queryString);
        query.setParameter("login", login);
        query.executeUpdate();
    }

    @Override
    public Optional<ApiRequest> findByLogin(String login) {
        return Optional.empty();
    }
}
