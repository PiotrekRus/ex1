package com.company.exercise.user;

public interface UserApi {
    User getCalculatedUserInfo(String login);
}
