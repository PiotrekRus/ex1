package com.company.exercise.user;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
public class ApiRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "LOGIN", unique = true)
    private String login;
    @Column(name = "REQUEST_COUNT")
    private Long count = 1L;

    ApiRequest incrementCount() {
        this.setCount(this.getCount() + 1);
        return this;
    }

    public ApiRequest(String login, Long id) {
        this.setId(id);
        this.setLogin(login);
        this.setCount(1L);
    }

}
