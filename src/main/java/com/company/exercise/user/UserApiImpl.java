package com.company.exercise.user;

import com.company.exercise.github_client.GithubApi;

class UserApiImpl implements UserApi {

    @Override
    public User getCalculatedUserInfo(String login) {
        databaseApi.saveRequest(login);
        var githubUser = githubApi.getGithubUserData(login);
        return githubUser.map(User::of).orElseThrow(UserNotFoundException::new);
    }

    private final GithubApi githubApi;
    private final DatabaseApi databaseApi;

    public UserApiImpl(GithubApi githubApi, DatabaseApi databaseAPI) {
        this.githubApi = githubApi;
        this.databaseApi = databaseAPI;
    }
}
