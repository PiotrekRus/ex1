package com.company.exercise.user

import com.company.exercise.github_client.GithubUser
import groovy.json.JsonSlurper

class UserModels {

    static def createGithubUser() {
        def jsonSlurper = new JsonSlurper()
        GithubUser user = (GithubUser) jsonSlurper.parseText('{\n' +
                '"login": "octocat",\n' +
                '"id": 583231,\n' +
                '"nodeId": "MDQ6VXNlcjU4MzIzMQ==",\n' +
                '"avatarUrl": "https://avatars3.githubusercontent.com/u/583231?v=4",\n' +
                '"gravatarId": "",\n' +
                '"url": "https://api.github.com/users/octocat",\n' +
                '"htmlUrl": "https://github.com/octocat",\n' +
                '"followersUrl": "https://api.github.com/users/octocat/followers",\n' +
                '"followingUrl": "https://api.github.com/users/octocat/following{/other_user}",\n' +
                '"gistsUrl": "https://api.github.com/users/octocat/gists{/gist_id}",\n' +
                '"starredUrl": "https://api.github.com/users/octocat/starred{/owner}{/repo}",\n' +
                '"subscriptionsUrl": "https://api.github.com/users/octocat/subscriptions",\n' +
                '"organizationsUrl": "https://api.github.com/users/octocat/orgs",\n' +
                '"reposUrl": "https://api.github.com/users/octocat/repos",\n' +
                '"eventsUrl": "https://api.github.com/users/octocat/events{/privacy}",\n' +
                '"receivedEventsUrl": "https://api.github.com/users/octocat/received_events",\n' +
                '"type": "User",\n' +
                '"siteAdmin": false,\n' +
                '"name": "The Octocat",\n' +
                '"company": "GitHub",\n' +
                '"blog": "http://www.github.com/blog",\n' +
                '"location": "San Francisco",\n' +
                '"email": null,\n' +
                '"hireable": null,\n' +
                '"bio": null,\n' +
                '"publicRepos": 8,\n' +
                '"publicGists": 8,\n' +
                '"followers": 2937,\n' +
                '"following": 9,\n' +
                '"createdAt": "2011-01-25T18:44:36Z",\n' +
                '"updatedAt": "2020-01-31T08:56:40Z"\n' +
                '}')
        return user
    }

    static def expectedUser() {
        return new User(583231, "octocat", "The Octocat", "User",
                "https://avatars3.githubusercontent.com/u/583231?v=4", "2011-01-25T18:44:36Z",
                0.020429009193054137)
    }
}
