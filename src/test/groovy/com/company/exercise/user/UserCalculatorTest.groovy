package com.company.exercise.user


import spock.lang.Specification

class UserCalculatorTest extends Specification {
    def random = new Random()

    def 'should return null if followers equals Zero'() {
        given: 'zero followers'
            def followers = 0
            def publicRepos = random.nextLong()
        when:
            def result = UserCalculator.calculate(followers, publicRepos)
        then:
            result == null
    }

    def 'should return null if publicRepos equals minus two'() {
        given: 'publicRepos equals minus two'
            def followers = random.nextLong()
            def publicRepos = -2L
        when:
            def result = UserCalculator.calculate(followers, publicRepos)
        then:
            result == null
    }
}
