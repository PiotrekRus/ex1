package com.company.exercise.user


import spock.lang.Specification

class UserApiImplTest extends Specification {

    def githubClient = Mock(com.company.exercise.github_client.GithubApi)
    def repository = new InMemoryRepository()
    def api = UserConfig.getUserApi(githubClient, repository)

    def 'should return user'() {
        given: 'github client returns valid user'
            githubClient.getGithubUserData("octocat") >> Optional.of(UserModels.createGithubUser())
        when: 'call api to return user'
            def user = api.getCalculatedUserInfo("octocat")
        then: 'user has valid fields'
            checkUserFieldsValid(user, UserModels.expectedUser())
    }

    def 'should throw UserNotFoundException when user is not found'() {
        given: 'github client returns Optional.empty'
            githubClient.getGithubUserData("octocat") >> Optional.empty()
        when: 'call api to return user'
            api.getCalculatedUserInfo("octocat")
        then: 'exception expected'
            thrown(UserNotFoundException)
    }

    def 'should add request info to database when call api.getCalculatedUserInfo'() {
        given:
            def login = 'octocat'
            githubClient.getGithubUserData("octocat") >> Optional.of(UserModels.createGithubUser())
        when: 'call api'
            api.getCalculatedUserInfo(login)
        then: 'request info should be in database'
            repository.findByLogin(login).isPresent()
    }

    def 'should increment count when request info exists in database'() {
        given: 'request info in database'
            repository.createOrUpdate("octocat")
            githubClient.getGithubUserData("octocat") >> Optional.of(UserModels.createGithubUser())
        when: 'call api'
            api.getCalculatedUserInfo('octocat')
        then: 'count should be 2'
            repository.findByLogin('octocat').get().count == 2L
    }

    private static void checkUserFieldsValid(User user, User expectedUser) {
        assert user.id == expectedUser.id
        assert user.login == expectedUser.login
        assert user.name == expectedUser.name
        assert user.type == expectedUser.type
        assert user.avatarUrl == expectedUser.avatarUrl
        assert user.createdAt == expectedUser.createdAt
        assert user.calculations == expectedUser.calculations
    }


}
