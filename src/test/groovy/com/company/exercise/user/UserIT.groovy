package com.company.exercise.user


import org.hamcrest.Matchers
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.TestConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Primary
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification
import spock.mock.DetachedMockFactory

import static UserModels.expectedUser
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@AutoConfigureMockMvc
class UserIT extends Specification {

    @Autowired
    MockMvc mvc

    @Autowired
    com.company.exercise.github_client.GithubApi githubApi


    def 'should return user json'() {
        given: 'github api returns valid user'
            githubApi.getGithubUserData("octocat") >> Optional.of(UserModels.createGithubUser())
        when: 'call /users/{login}'
            def result = mvc.perform(get("/users/{login}", "octocat"))
        then: 'api returns user json'
            result
                    .andExpect(status().is2xxSuccessful())
                    .andExpect(jsonPath('$.id').value(expectedUser().id))
                    .andExpect(jsonPath('$.login').value(expectedUser().login))
                    .andExpect(jsonPath('$.name').value(expectedUser().name))
                    .andExpect(jsonPath('$.avatarUrl').value(expectedUser().avatarUrl))
                    .andExpect(jsonPath('$.createdAt').value(expectedUser().createdAt))
                    .andExpect(jsonPath('$.calculations').value(expectedUser().calculations))
    }

    def 'should return 404 status when user is not found'() {
        given: 'github api returns Optional.empty'
            githubApi.getGithubUserData("octocat") >> Optional.empty()
        when: 'call /users/{login}'
            def result = mvc.perform(get("/users/{login}", "octocat"))
        then: 'api returns status 404 with message'
            result
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath('$.message').value(Matchers.isA(String.class)))
    }

    @TestConfiguration
    static class MockConfig {
        def mockFactory = new DetachedMockFactory()

        @Bean
        @Primary
        com.company.exercise.github_client.GithubApi githubApiMock() {
            return mockFactory.Mock(com.company.exercise.github_client.GithubApi)
        }
    }
}
