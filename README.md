# RecruitmentExercise

### Goals
Stwórz prosty RESTowy serwis, który zwróci informacje
 Identyfikator
 Login
 Nazwa
 Typ
 Url do avatara
 Data stworzenia
 Obliczenia
API serwisu powinno wyglądać jak poniżej:
GET /users/{login}
{
&quot;id&quot;: &quot;...&quot;,
&quot;login&quot;: &quot;...&quot;,
&quot;name&quot;: &quot;…&quot;,
&quot;type&quot;: &quot;...&quot;,
&quot;avatarUrl&quot;: „”,
&quot;createdAt&quot;: &quot;...&quot;
&quot;calculations&quot;: &quot;...&quot;
}
Serwis powinien pobrać dane z https://api.github.com/users/{login} (np.
https://api.github.com/users/octocat) i przekazać je w API. W polu calculations powinien być
zwrócony wynik działania 6 / liczba_followers * (2 + liczba_public_repos).
Serwis powinien zapisywać w bazie danych liczbę wywołań API dla każdego loginu.
Baza danych powinna zawierać dwie kolumny: LOGIN oraz REQUEST_COUNT. Dla każdego wywołania
usługi wartość REQUEST_COUNT powinna być zwiększana o jeden.
Serwis powinien być zaimplementowany w Java. Projekt powinien być możliwy do zbudowania za
pomocą Maven lub Gradle. Możesz wspierać się dowolnymi, łatwo dostępnymi technologiami (silniki
BD, biblioteki, frameworki).
Pamiętaj o zastosowaniu dobrych praktyk programowania.
Projekt umieść na dowolnym repozytorium i udostępnij nam link.

### Requirements: 
JDK11, maven

### Run project instruction:

* clone repo
* mvn test install
* cd /target
* java -jar exercise-0.0.1-SNAPSHOT.jar


For connect to H2 database admin panel open url [http://localhost:8080/h2-console](http://localhost:8080/h2-console)

### Fill fields with value:

* JDBC URL: jdbc:h2:mem:exercise
* Username: sa 
* Password: password

### Swagger

Swagger is available at url [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
